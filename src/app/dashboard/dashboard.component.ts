import { Component, OnInit } from '@angular/core';
import { MatchType } from '../models/match-type';
import { MATCHTYPES } from '../mock_data/match-type';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	matchTypes: MatchType[] = []
  selectedType: MatchType

	onSelectType (matchType: MatchType): void {
	  this.selectedType= matchType;
	}

  constructor() { 
    this.matchTypes=  MATCHTYPES;
  }

  ngOnInit() {
  
  }

}
