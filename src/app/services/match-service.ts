import { Player } from '../models/player';

/*
*
* Check which item win the match and return that item. 
* In equal case the function return false
*
*/

function whoWin(playerLeft: Player, playerRight: Player) {
	let winner;

    if(playerLeft.item.id == playerRight.item.id){
    	//equal
    	winner = new Player();
    }else if(playerLeft.item.id == playerRight.item.killedby){
    	//playerLeft.item win
    	winner = playerLeft;
    }else if(playerRight.item.id == playerLeft.item.killedby){
    	//playerRight.item win
    	winner = playerRight;
    }
    return winner
}

export {whoWin};