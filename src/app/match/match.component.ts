import { Component, OnInit } from '@angular/core';

import { Item } from '../models/item';
import { Player } from '../models/player';
import { Match } from '../models/match';
import { ITEMS } from '../mock_data/items';
import { whoWin } from '../services/match-service';
import { PLAYERLEFT } from '../mock_data/players';
import { PLAYERRIGHT } from '../mock_data/players';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

	items
	infoLog

	selectedItemPlayerRight: Item
	playerLeft: Player
	playerRight: Player
	winner: Player
	history: Match[] = []

	onSelectItem (item: Item): void {
	    let index= Math.floor(Math.random() * 3);

		this.playerLeft.item= item;
	    this.playerRight.item= this.items[index];
	    this.winner = whoWin(this.playerLeft, this.playerRight);

	    this.history.push(new Match(this.playerLeft, this.playerRight, this.winner));
	}

	onResetMatch (): void {
		this.winner = new Player();
	    this.playerRight.item= new Item();
	    this.history = [];
	}

	getTotalMatchesWinner (player: Player): number {
		
		var winnerMatch = this.history.filter(function(item) {
		  return item.winner.id === player.id;
		});
		return winnerMatch.length;
	}

	constructor() { 

		this.winner=  new Player();
		this.items=  ITEMS;
		this.playerLeft=  PLAYERLEFT;
		this.playerRight=  PLAYERRIGHT;
	}

	ngOnInit() {

	}

}
