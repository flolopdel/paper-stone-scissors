import { Item } from './item';

export class Player {
  id: number;
  name: string;
  item: Item;

  constructor(id= 0, name= "", item= new Item()) {
    this.id = id;
    this.name = name;
    this.item = item;
  }
}
