export class Item {
  id: number;
  name: string;
  src: string;
  killedby: number;

  constructor(id= 0, name= "", src="", killedby= 0) {
    this.id = id;
    this.name = name;
    this.src = src;
    this.killedby = killedby;
  }
}
