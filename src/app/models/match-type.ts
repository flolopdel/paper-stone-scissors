import { Match } from './match';

export class MatchType {
  id: number;
  name: string;
  disabled: boolean;

  constructor(id= 0, name= "", disabled= false) {
    this.id = id;
    this.name = name;
    this.disabled = disabled;
  }
}