import { Player } from './player';
import { MatchType } from './match-type';

export class Match {
  playerLeft: Player;
  playerRight: Player;
  winner: Player;
  matchType: MatchType;

  constructor(playerLeft= new Player(), playerRight= new Player(), winner=new Player(), matchType=new MatchType()) {
    this.playerLeft = playerLeft;
    this.playerRight = playerRight;
    this.winner = winner;
    this.matchType = matchType;
  }
}