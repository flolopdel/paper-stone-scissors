import { Player } from '../models/player';
import { Item } from '../models/item';

export const PLAYERLEFT: Player = { id: 1, name: 'Player 1', item: new Item()};
export const PLAYERRIGHT: Player = { id: 2, name: 'Computer', item: new Item()};