import { MatchType } from '../models/match-type';

export const MATCHTYPES: MatchType[] = [
  { id: 1, name: 'Computer', disabled: false },
  { id: 2, name: 'Remote', disabled: true }
];
