import { Item } from '../models/item';

export const ITEMS: Item[] = [
  { id: 1, name: 'Paper', src: 'paper.jpg', killedby: 3 },
  { id: 2, name: 'Stone', src: 'stone.jpg', killedby: 1 },
  { id: 3, name: 'Scissors', src: 'scissors.jpg', killedby: 2 },
];
