# Paper, stone or scissors

This project was generated with [Angular CLI](https://github.com/angular/angular-cli). It use docker as environment manager.

## Development server

There are 2 way to install this project

### Using Docker

Run `docker-compose up`.
Navigate to `http://localhost:4200/`.

### Without Docker

Run next commands
```
cd to/project/folder
npm install
npm install -g @angular/cli
npm run start
```
Navigate to `http://localhost:4200/`
